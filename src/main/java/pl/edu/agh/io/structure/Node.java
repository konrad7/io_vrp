package pl.edu.agh.io.structure;


public class Node {

    private String customerId = "";
    private int x;
    private int y;
    private int demand = 0;
    private int windowTimeStart = 0;
    private int windowTimeEnd = Integer.MAX_VALUE;
    private int serviceTime = 0;

    public Node(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public Node(int x, int y, int demand) {
        this(x, y);
        this.demand = demand;
    }

    public double distance(Node node) {
        return Math.sqrt(Math.pow((node.getX() - x), 2) + Math.pow((node.getY() - y), 2));
    }

    private double getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    private double getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public int getDemand() {
        return demand;
    }

    public void setDemand(int demand) {
        this.demand = demand;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public int getWindowTimeStart() {
        return windowTimeStart;
    }

    public void setWindowTimeStart(int windowTimeStart) {
        this.windowTimeStart = windowTimeStart;
    }

    public int getWindowTimeEnd() {
        return windowTimeEnd;
    }

    public void setWindowTimeEnd(int windowTimeEnd) {
        this.windowTimeEnd = windowTimeEnd;
    }

    public int getServiceTime() {
        return serviceTime;
    }

    public void setServiceTime(int serviceTime) {
        this.serviceTime = serviceTime;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Node node = (Node) o;

        if (x != node.x) return false;
        return y == node.y;
    }

    @Override
    public int hashCode() {
        int result = x;
        result = 31 * result + y;
        return result;
    }

    @Override
    public String toString() {
        return "Node{" +
                "customerId='" + customerId + '\'' +
                ", x=" + x +
                ", y=" + y +
                ", demand=" + demand +
                ", windowTimeStart=" + windowTimeStart +
                ", windowTimeEnd=" + windowTimeEnd +
                ", serviceTime=" + serviceTime +
                '}';
    }
}