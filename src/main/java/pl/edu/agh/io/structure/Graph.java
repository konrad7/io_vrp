package pl.edu.agh.io.structure;

import pl.edu.agh.io.util.Generator;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Graph {

    private static final Pattern PATTERN = Pattern.compile("^\\((\\d+), (\\d+)\\) (\\d+)$");
    private static final int VEHICLE_NUMBER = 10;

    private Node nodes[];
    private int graphDimension;
    private int vehicleCapacity;

    private Graph() {
    }

    public static Graph build() {
        return build(null, Type.TEST);
    }

    public static Graph build(String fileName, Type type) {
        Graph graph = new Graph();
        if (Type.TEST == type) {
            graph.buildTestGraph();
        } else if (Type.FILE == type) {
            graph.buildGraphFromFile(fileName);
        } else if (Type.SOLOMON == type) {
            graph.buildGraphFromSolomonSample(fileName);
        }
        return graph;
    }

    private void buildTestGraph() {
        this.graphDimension = 10;
        this.vehicleCapacity = 100;
        this.nodes = new Node[getGraphDimension()];
        this.nodes[0] = new Node(0, 0);
        this.nodes[1] = new Node(1, 1);
        this.nodes[2] = new Node(2, 2);
        this.nodes[3] = new Node(4, 0);
        this.nodes[4] = new Node(-1, 1);
        this.nodes[5] = new Node(-3, 1);
        this.nodes[6] = new Node(-1, -2);
        this.nodes[7] = new Node(-1, -1);
        this.nodes[8] = new Node(-1, -4);
        this.nodes[9] = new Node(1, -2);
        for (int i = 0; i < getGraphDimension(); i++) {
            this.nodes[i].setDemand(1);
        }
    }

    private void buildGraphFromFile(String filename) {
        List<Node> nodes = new ArrayList<>();
        try (BufferedReader buffer = new BufferedReader(new FileReader(Generator.CATALOG_PATH + filename))) {
            String line;
            while ((line = buffer.readLine()) != null) {
                nodes.add(createNode(line));
            }
        } catch (IOException | ParseException e) {
            e.printStackTrace();
        }
        this.graphDimension = nodes.size();
        this.vehicleCapacity = 100;
        this.nodes = nodes.toArray(new Node[]{});
    }

    private void buildGraphFromSolomonSample(String filename) {
        List<Node> nodes = new ArrayList<>();
        int vehicleCapacity = 100;
        int counter = 0;
        try (BufferedReader buffer = new BufferedReader(new FileReader(Generator.CATALOG_PATH + filename))) {
            String line;
            while ((line = buffer.readLine()) != null) {
                String[] tokens = line.replace("\r", "").trim().split(" +");
                if (++counter == 5) {
                    vehicleCapacity = Integer.parseInt(tokens[1]);
                } else if (counter > 9 && tokens.length >= 7) {
                    Node node = new Node(Integer.parseInt(tokens[1]), Integer.parseInt(tokens[2]));
                    node.setDemand(Integer.parseInt(tokens[3]));
                    node.setCustomerId(tokens[0]);
                    node.setWindowTimeStart(Integer.parseInt(tokens[4]));
                    node.setWindowTimeEnd(Integer.parseInt(tokens[5]));
                    node.setServiceTime(Integer.parseInt(tokens[6]));
                    nodes.add(node);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        this.vehicleCapacity = vehicleCapacity;
        this.graphDimension = nodes.size();
        this.nodes = nodes.toArray(new Node[]{});
    }

    public int getGraphDimension() {
        return graphDimension;
    }

    public int getVehicleCapacity() {
        return vehicleCapacity;
    }

    public int getVehicleNumber() {
        return VEHICLE_NUMBER;
    }

    public Node[] getNodes() {
        return nodes;
    }

    private Node createNode(String line) throws ParseException {
        Matcher matcher = PATTERN.matcher(line);
        if (matcher.find()) {
            int x = Integer.valueOf(matcher.group(1));
            int y = Integer.valueOf(matcher.group(2));
            int demand = Integer.valueOf(matcher.group(3));
            return new Node(x, y, demand);
        }
        throw new ParseException("Invalid format of data", 0);
    }

    public void print() {
        System.out.println("GRAPH_DIMENSION: " + getGraphDimension());
        System.out.println("VEHICLE_CAPACITY: " + getVehicleCapacity());
        System.out.println("VEHICLE_NUMBER: " + getVehicleNumber());

        for (int i = 0; i < getGraphDimension(); i++) {
            System.out.println("Node " + i + ": " + nodes[i]);
        }
        System.out.println();
    }

    public enum Type {
        TEST, FILE, SOLOMON;
    }

}