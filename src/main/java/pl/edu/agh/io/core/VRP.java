package pl.edu.agh.io.core;

import org.jgap.*;
import org.jgap.impl.DefaultConfiguration;
import org.jgap.impl.IntegerGene;
import pl.edu.agh.io.structure.Graph;
import pl.edu.agh.io.structure.Node;

import java.util.LinkedList;

public class VRP {

    private static final int MAX_EVOLUTIONS = 1000;
    private static final int POPULATION_SIZE = 30;
    private final Graph graph;

    public VRP(Graph graph) {
        this.graph = graph;
    }

    public void run() {
        try {
            Configuration conf = createConfiguration();
            Genotype genotype = Genotype.randomInitialGenotype(conf);
            processEvolutions(genotype);
            printResults(genotype);
        } catch (InvalidConfigurationException e) {
            e.printStackTrace();
        }

    }

    private Configuration createConfiguration() throws InvalidConfigurationException {
        Configuration conf = new DefaultConfiguration();
        Fitness fitness = new Fitness(graph);
        Gene[] sampleGenes = createSampleGenes(conf, graph);
        IChromosome sampleChromosome = new Chromosome(conf, sampleGenes);

        conf.setPreservFittestIndividual(true);
        conf.setFitnessFunction(fitness);
        conf.setSampleChromosome(sampleChromosome);
        conf.setPopulationSize(POPULATION_SIZE);
        return conf;
    }

    private Gene[] createSampleGenes(Configuration conf, Graph graph) throws InvalidConfigurationException {
        Gene[] sampleGenes = new Gene[graph.getGraphDimension()];
        for (int i = 0; i < graph.getGraphDimension(); i++) {
            sampleGenes[i] = new IntegerGene(conf, 0, graph.getVehicleNumber() - 1);
        }
        return sampleGenes;
    }

    private void processEvolutions(Genotype genotype) {
        long startTime = System.currentTimeMillis();
        for (int i = 0; i < MAX_EVOLUTIONS; i++) {
//            System.out.print((i % 1000 == 0) ? "\n" : (i % 10 == 0) ? "." : "");
            if (!uniqueChromosomes(genotype.getPopulation())) {
                throw new RuntimeException("Invalid generation in the evolution " + i);
            }
            genotype.evolve();

            IChromosome bestSolution = genotype.getFittestChromosome();
            Double bestDistance = 0.0;
            for (int j = 0; j < graph.getVehicleNumber(); j++) {
                bestDistance += Fitness.getDistance(j, bestSolution, graph);
            }
            System.out.println("Iteration " + (i + 1) + ": fitness = " + bestSolution.getFitnessValue() + " distance = " + bestDistance);
        }
        long endTime = System.currentTimeMillis();

        System.out.println();
        System.out.println("Number of the evolutions: " + MAX_EVOLUTIONS);
        System.out.println("Total time: " + (endTime - startTime) + "ms");
    }

    private boolean uniqueChromosomes(Population population) {
        for (int i = 0; i < population.size() - 1; i++) {
            IChromosome c = population.getChromosome(i);
            for (int j = i + 1; j < population.size(); j++) {
                IChromosome c2 = population.getChromosome(j);
                if (c == c2) {
                    return false;
                }
            }
        }
        return true;
    }

    private void printResults(Genotype genotype) {
        IChromosome bestSolution = genotype.getFittestChromosome();
        System.out.println("The best fitness solution: " + bestSolution.getFitnessValue());
        bestSolution.setFitnessValueDirectly(-1);
        System.out.println();

//        System.out.println("Result: ");
//        for (int i = 0; i < graph.getGraphDimension(); i++) {
//            System.out.println(i + ". " + Fitness.getNumberAtGene(bestSolution, i));
//        }
//        System.out.println();

        Double bestDistance = 0.0;
        for (int i = 0; i < graph.getVehicleNumber(); i++) {
            Double routeDistance = Fitness.getDistance(i, bestSolution, graph);
            LinkedList<Integer> routes = Fitness.getPositions(i, bestSolution, graph);
            System.out.println("Route #" + (i + 1) + ": " + routes + " - Distance: " + routeDistance);
            bestDistance += routeDistance;
        }
        System.out.println("The best distance: " + bestDistance);
        System.out.println();

        System.out.println("DETAILS:");
        System.out.println();
        System.out.println("Vehicle capacity = " + graph.getVehicleCapacity());

        Node startNode = graph.getNodes()[0];
        for (int i = 0; i < graph.getVehicleNumber(); i++) {
            System.out.println("Route #" + (i + 1) + ":");
            int time = startNode.getWindowTimeStart();
            int capacity = 0;

            printNodeDetailsWithTime(startNode, time);
            LinkedList<Integer> routes = Fitness.getPositions(i, bestSolution, graph);
            for (int r : routes) {
                Node nextNode = graph.getNodes()[r];
                capacity += nextNode.getDemand();

                printNodeDetailsWithTime(nextNode, time);
                if (time < graph.getNodes()[r].getWindowTimeStart()) {
                    time = graph.getNodes()[r].getWindowTimeStart();
                }
                time += graph.getNodes()[r].getServiceTime();
            }
            printNodeDetailsWithTime(startNode, time);
            System.out.println("Total capacity = " + capacity);
            System.out.println();
        }
    }

    private void printNodeDetailsWithTime(Node node, int time) {
        String note = (time < node.getWindowTimeStart()) ? "EARLY" : (time > node.getWindowTimeEnd()) ? "LATE" : "ON TIME";
        System.out.println(node + " - arriveTime = " + time + " [" + note + "]");
    }

}