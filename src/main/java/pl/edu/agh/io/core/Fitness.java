package pl.edu.agh.io.core;

import org.jgap.FitnessFunction;
import org.jgap.IChromosome;
import pl.edu.agh.io.structure.Graph;
import pl.edu.agh.io.structure.Node;

import java.util.LinkedList;

public class Fitness extends FitnessFunction {

    private static final double DISTANCE_WEIGHT = 10.0;
    private static final double CAPACITY_WEIGHT = 8.0;
    private static final double TIME_WEIGHT = 1.0;
    private static final double POINTS_FOR_EXCEEDING_CAPACITY = 20.0;
    private static final double POINTS_FOR_FREE_CAPACITY = 1.0;
    private static final double POINTS_FOR_BEING_TOO_EARLY = 0.2;
    private static final double POINTS_FOR_BEING_TOO_LATE = 0.6;
    private static final double POINTS_FOR_BEING_ON_TIME = 0.1;
    private final Graph graph;


    public Fitness(Graph graph) {
        this.graph = graph;
    }

    public static double getDistance(int vehicleNumber, IChromosome chromosome, Graph graph) {
        double totalDistance = 0.0;
        LinkedList<Integer> positions = getPositions(vehicleNumber, chromosome, graph);
        Node deposit = graph.getNodes()[0];
        Node lastVisit = deposit;

        while (!positions.isEmpty()) {
            int pos = positions.pop();
            Node visit = graph.getNodes()[pos];
            totalDistance += lastVisit.distance(visit);
            lastVisit = visit;
        }

        totalDistance += lastVisit.distance(deposit);
        return totalDistance;
    }

    public static LinkedList<Integer> getPositions(int vehicleNumber, IChromosome chromosome, Graph graph) {
        LinkedList<Integer> p = new LinkedList<>();
        for (int i = 1; i < graph.getGraphDimension(); i++) {
            int valueChromosome = (Integer) chromosome.getGene(i).getAllele();
            if (valueChromosome == vehicleNumber) {
                p.add(i);
            }
        }
        return p;
    }

    public static double getNumberAtGene(IChromosome potentialSolution, int position) {
        return (Integer) potentialSolution.getGene(position).getAllele();
    }

    @Override
    public double evaluate(IChromosome chromosome) {
        double fitness = 0.0;
        for (int i = 0; i < graph.getVehicleNumber(); i++) {
            fitness += getDistance(i, chromosome, graph) * DISTANCE_WEIGHT;
            fitness += getCapacity(i, chromosome) * CAPACITY_WEIGHT;
            fitness += getTimeWindow(i, chromosome) * TIME_WEIGHT;
        }
        return fitness < 0.0 ? 0.0 : 1000000.0 - fitness;
    }

    private double getCapacity(int vehicleNumber, IChromosome chromosome) {
        int vehicleCapacity = graph.getVehicleCapacity();
        double demandTotal = 0.0;
        LinkedList<Integer> positions = getPositions(vehicleNumber, chromosome, graph);
        while (!positions.isEmpty()) {
            int pos = positions.pop();
            Node visit = graph.getNodes()[pos];
            demandTotal += visit.getDemand();
        }
        if (demandTotal > vehicleCapacity) {
            return (demandTotal - vehicleCapacity) * POINTS_FOR_EXCEEDING_CAPACITY;
        }
        return (vehicleCapacity - demandTotal) * POINTS_FOR_FREE_CAPACITY;
    }

    private double getTimeWindow(int vehicleNumber, IChromosome chromosome) {
        double points = 0.0;
        LinkedList<Integer> positions = getPositions(vehicleNumber, chromosome, graph);
        Node deposit = graph.getNodes()[0];
        int totalTime = deposit.getWindowTimeStart();

        while (!positions.isEmpty()) {
            int pos = positions.pop();
            Node visit = graph.getNodes()[pos];

            if (totalTime < visit.getWindowTimeStart()) {
                // waiting until ready, so waisting time
                points += (visit.getWindowTimeStart() - totalTime) * POINTS_FOR_BEING_TOO_EARLY;
                totalTime = visit.getWindowTimeStart();
            } else if (totalTime >= visit.getWindowTimeStart() && totalTime <= visit.getWindowTimeEnd()) {
                points += (totalTime - visit.getWindowTimeStart()) * POINTS_FOR_BEING_ON_TIME;
            } else {
                // too late
                points += (totalTime - visit.getWindowTimeEnd()) * POINTS_FOR_BEING_TOO_LATE;
            }
            totalTime += visit.getServiceTime();
        }

        if (totalTime > deposit.getWindowTimeEnd()) {
            // late return
            points += (totalTime - deposit.getWindowTimeEnd()) * POINTS_FOR_BEING_TOO_LATE;
        }

        return points;
    }
}