package pl.edu.agh.io;

import pl.edu.agh.io.core.VRP;
import pl.edu.agh.io.structure.Graph;

public class Main {

    public static void main(String[] args) throws Exception {
//        Graph graph = Graph.build(); // Test graph with hardcoded 10 nodes
//        Graph graph = Graph.build("test1.txt", Graph.Type.FILE); // Graph read from file (structure (x, y) demand
        Graph graph = Graph.build("C101_solomon.txt", Graph.Type.SOLOMON); // Solomon graph read from file
//        graph.print();

        VRP vrp = new VRP(graph);
        vrp.run();
    }
}
