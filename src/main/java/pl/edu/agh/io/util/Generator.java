package pl.edu.agh.io.util;

import pl.edu.agh.io.structure.Node;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.*;

public class Generator {

    public static final String CATALOG_PATH = "src/main/resources/samples/";

    private static final String FILE_NAME = "test1.txt";
    private static final int NUMBER_OF_NODES = 10;
    private static final int X_RANGE = 301; // 0 - 300
    private static final int Y_RANGE = 301; // 0 - 300
    private static final int DEMAND_RANGE = 10; // 1 - 10

    public static void main(String[] args) {
        List<Node> nodes = generateUniqueNode();
        saveToFile(nodes);
    }

    private static List<Node> generateUniqueNode() {
        Set<Node> uniqueNodes = new HashSet<>();
        Random random = new Random();

        while (uniqueNodes.size() < NUMBER_OF_NODES) {
            int x = random.nextInt(X_RANGE);
            int y = random.nextInt(Y_RANGE);
            int demand = random.nextInt(DEMAND_RANGE) + 1;
            uniqueNodes.add(new Node(x, y, demand));
        }

        return new ArrayList<>(uniqueNodes);
    }

    private static void saveToFile(List<Node> nodes) {
        try (BufferedWriter buffer = new BufferedWriter(new FileWriter(CATALOG_PATH + FILE_NAME))) {
            for (Node node : nodes) {
                buffer.write(node.toString() + "\n");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
